# Advent of Code - Day 20 - Part One
from dataclasses import dataclass
from pprint import pprint
import numpy as np
import re
from collections import deque


def result(input):
    input_tiles = []
    for tile_input in "\n".join(input).split("\n\n"):
        parts = tile_input.split("\n")

        m = re.search("Tile (\d+):", parts[0])
        id = int(m.group(1))

        contents = np.array(list("".join(parts[1:]))).reshape(10, 10)

        tile = Tile(id, contents)
        input_tiles.append(tile)

    pprint(input_tiles)

    start = input_tiles[0]
    unmatched = deque(input_tiles[1:])

    while(len(unmatched) > 0):
        next_tile_to_test = unmatched.popleft()

        # " get all orientations of this next_tile_to_test

        # " check whether the edge matches one the tiles already placed
        #
        # if so:
        #       " place the tile
        # else:
        #       " try the tile later and try another one
        #       unmatched.append(next_tile_to_test)

    top_left = start


    return input

class Tile:
    id: int
    contents: np.ndarray
    top: None
    left: None
    right: None
    down: None

    def __init__(self, id, contents):
        self._id = id
        self._contents = contents

