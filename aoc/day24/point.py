_directions = ["e", "w", "ne", "se", "nw", "sw"]


def move(point, direction):
    (x,y) = point

    if direction == "e":
        return x+1, y
    elif direction == "w":
        return x-1, y
    elif direction == "ne":
        if y % 2 == 0:
            return x, y -1
        return x + 1, y - 1
    elif direction == "se":
        if y % 2 == 0:
            return x, y + 1
        return x + 1, y + 1
    elif direction == "nw":
        if y % 2 == 0:
            return x - 1, y - 1
        return x, y - 1
    elif direction == "sw":
        if y % 2 == 0:
            return x - 1, y + 1
        return x, y + 1

    raise 'unknown direction'

def setup_tiles(input):

    tiles = {}

    for movement in input:
        current_point = (3,3)
        while len(movement) > 0:
            for direction in _directions:
                if movement.startswith(direction):
                    current_point = move(current_point, direction)
                    movement = movement[slice(len(direction),None)]
                    break
        if current_point not in tiles:
            tiles[current_point] = False  # == White
        tiles[current_point] = not tiles[current_point]

    return tiles

def neighbours(point):
    return [move(point, dir) for dir in _directions]
