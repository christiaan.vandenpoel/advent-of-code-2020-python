# Advent of Code - Day 24 - Part Two
from pprint import pprint

from aoc.day24 import point


def result(input, nbr_of_days):
    tiles = point.setup_tiles(input)

    while nbr_of_days > 0:
        nbr_of_days -= 1
        new_tiles = {}
        to_test = set()

        for coordinate, is_black in tiles.items():
            to_test |= set(point.neighbours(coordinate) + [coordinate])

        for coordinate in to_test:
            is_black = tiles.get(coordinate, False)
            count_black = 0
            for neighbour in point.neighbours(coordinate):
                if tiles.get(neighbour, False):
                    count_black += 1

            if is_black:
                if count_black == 0 or count_black > 2:
                    pass # becomse white
                else:
                    new_tiles[coordinate] = True
            else:
                if count_black == 2:
                    new_tiles[coordinate] = True

        tiles = new_tiles

    count = 0

    for key, value in tiles.items():
        if value:
            count += 1

    return count
