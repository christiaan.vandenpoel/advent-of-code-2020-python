# Advent of Code - Day 24 - Part One
from pprint import pprint
from aoc.day24 import point

def result(input):
    tiles = point.setup_tiles(input)

    count = 0

    for key, value in tiles.items():
        if value:
            count += 1

    return count

