# Advent of Code - Day 16 - Part Two
from .ticket_parser import TicketParser

def result(input, pattern):
    tp = TicketParser(input)
    return tp.multiply_values_for_field(pattern)
