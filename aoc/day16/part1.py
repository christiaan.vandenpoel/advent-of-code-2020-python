# Advent of Code - Day 16 - Part One
from .ticket_parser import TicketParser

def result(input):
    return TicketParser(input).accumulate_invalid_values()
