import re


class TicketParser:
    def __init__(self, input):
        self._input = input
        self._ranges = []
        self._nearby_tickets_list = []
        self._ranges_dict = {}

        self._parse_input()

    def _parse_input(self):
        state = "FIELDS"
        for row in self._input:
            if state == "FIELDS":
                if row != "":
                    # fields
                    field_name = row.split(": ")[0]
                    ranges_for_field = row.split(": ")[1].split(" or ")
                    for range in ranges_for_field:
                        low_high_range = list(map(lambda x: int(x), range.split('-')))
                        self._ranges.append(low_high_range)
                        if field_name not in self._ranges_dict:
                            self._ranges_dict[field_name] = []
                        self._ranges_dict[field_name].append(low_high_range)
                    pass
                else:
                    state = "YOUR_TICKET"
            elif state == "YOUR_TICKET":
                if row == "your ticket:":
                    pass
                elif row != "":
                    self._your_ticket = list(map(lambda x: int(x), row.split(',')))
                    pass
                else:
                    state = "NEARBY_TICKETS"
            elif state == "NEARBY_TICKETS":
                if row != "" and row != "nearby tickets:":
                    self._nearby_tickets_list.append(list(map(lambda x: int(x), row.split(","))))
                else:
                    pass

    def nearby_tickets(self):
        return self._nearby_tickets_list

    def value_fits_any_field(self, value):
        return any(x[0] <= value <= x[1] for x in self._ranges)

    def accumulate_invalid_values(self):
        accumulator = 0

        for nbticket in self.nearby_tickets():
            for value in nbticket:
                if not self.value_fits_any_field(value):
                    accumulator += value

        return accumulator

    def multiply_values_for_field(self, pattern):
        nbr_matches_per_column = {}

        # 1. discard invalid tickets
        valid_nearby_tickets = list(
            filter(lambda nbt: all(self.value_fits_any_field(nbtv) for nbtv in nbt), self._nearby_tickets_list))

        # for each column:
        #   find for each field how many rows matches
        total_columns = len(valid_nearby_tickets[0])
        for column_idx in range(0, total_columns):
            all_column_values = list(map(lambda x: x[column_idx], valid_nearby_tickets))

            for field_name, ranges in self._ranges_dict.items():
                nbr_matches_for_field = 0
                for value in all_column_values:
                    if any(range[0] <= value <= range[1] for range in ranges):
                        nbr_matches_for_field += 1

                if column_idx not in nbr_matches_per_column:
                    nbr_matches_per_column[column_idx] = {}
                nbr_matches_per_column[column_idx][field_name] = nbr_matches_for_field

        working_list = {}

        # remove all fields that don't match all rows
        for row, fields in nbr_matches_per_column.items():
            for field_name in fields.keys():
                if fields[field_name] == len(valid_nearby_tickets):
                    if row not in working_list:
                        working_list[row] = {}
                    working_list[row][field_name] = fields[field_name]

        mapped_columns = {}
        # {0: {'row': 3}, 1: {'class': 3, 'row': 3}, 2: {'class': 3, 'row': 3, 'seat': 3}}
        while len(mapped_columns) < len(self._ranges_dict.keys()):
            # find the column with only one possible field
            col = dict(filter(lambda item: len(working_list[item[0]]) == 1, working_list.items()))
            for col, fields in col.items():
                for key in fields.keys():
                    mapped_columns[col] = key

            # now remove all mapped_colums we already know
            for col, fields in working_list.items():
                for known_field in mapped_columns.values():
                    if known_field in fields.keys():
                        del (fields[known_field])

        accumulator = 1

        for col_idx, field_name in mapped_columns.items():
            if re.match(pattern, field_name) is not None:
                accumulator *= self._your_ticket[col_idx]

        return accumulator
