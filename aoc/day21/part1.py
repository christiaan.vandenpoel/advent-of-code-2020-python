# Advent of Code - Day 21 - Part One
from collections import defaultdict, Counter


def result(input):
    ingredients_list = []
    mapping = []
    for row in input:
        allergeen_list = []
        dest = list()
        ingredients_list.append(dest)
        for word in row.split():
            if word.startswith("("):
                dest = allergeen_list
                continue
            word = word.replace('(','').replace(')', '').replace(',', '')
            dest.append(word)

        mapping.append((ingredients_list[-1], allergeen_list))

    foods = defaultdict(list)
    allergen_ingredients = defaultdict(set)
    ingredient_counts = Counter()
    for ingredients, allergens in mapping:
        for ingredient in ingredients:
            ingredient_counts[ingredient] += 1
        for allergen in allergens:
            S = set()
            foods[allergen].append(S)
            for ingredient in ingredients:
                S.add(ingredient)
                allergen_ingredients[allergen].add(ingredient)

    for k, v in foods.items():
        allergen_ingredients[k].intersection_update(set.intersection(*v))

    ingredients_possibly_allergens = set.union(*allergen_ingredients.values())
    inert_ingredients = set(ingredient_counts).difference(ingredients_possibly_allergens)
    return sum(v for k, v in ingredient_counts.items() if k in inert_ingredients)