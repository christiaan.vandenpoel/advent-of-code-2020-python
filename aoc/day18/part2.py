# Advent of Code - Day 18 - Part Two
from collections import deque


def result(input):
    accumulator = 0
    for row in input:
        accumulator += calculate(row.replace(' ', ''))
    return accumulator

def calculate(input):
    tokens = _tokenize(input)
    rpn = _to_rpn(tokens)

    stack = deque()

    while(len(rpn)>0):
        n = rpn[0]
        rpn = rpn[1:]
        if n.isdigit():
            stack.append(n)
        elif n == '+':
            stack.append(int(stack.pop()) + int(stack.pop()))
        else:
            stack.append(int(stack.pop()) * int(stack.pop()))

    return stack.pop()

def _tokenize(input):
    stack = deque()

    for char in input:
        prev = None
        if len(stack) > 0 or None:
            prev = stack.pop() 

        if prev is not None and prev.isdigit() and char.isdigit():
            stack.append(prev+char)
        else:
            if prev is not None:
                stack.append(prev) 
            stack.append(char)

    return stack

def _to_rpn(tokens):
    stack = deque()
    queue = list()

    for token in tokens:
        if token.isdigit():
            queue.append(token)
        elif token in '+*':
            prev = stack.pop() if len(stack) > 0 else None
            if prev == '+':
                queue.append(prev) if prev is not None else None
                stack.append(token)
            else:
                stack.append(prev) if prev is not None else None
                stack.append(token)
        elif token == ')':
            prev = stack.pop()
            while prev != '(' and len(stack) > 0:
                queue.append(prev)
                if len(stack) > 0:
                    prev = stack.pop()
                else:
                    prev = None
        elif token == '(':
            stack.append(token)
        else:
            raise Exception("Don't know what to do here")

    while(len(stack) > 0):
        queue.append(stack.pop())

    return queue