# Advent of Code - Day 17 - Part One
from pprint import pprint

def result(input, cycles):
    result = get_points_from_input(input)
    while cycles > 0:
        result = cycle_points(result)
        cycles -= 1
    return len(result)


def create_point(x,y,z):
    return (x,y,z)

def get_neighbours(point):
    return [(x,y,z)
            for x in range(point[0] - 1, point[0] + 2)
            for y in range(point[1] - 1, point[1] + 2)
            for z in range(point[2] - 1, point[2] + 2)
            if create_point(x,y,z) != point]

def get_points_from_input(input):
    result = []
    for x, row in enumerate(input):
        for y, char in enumerate(row):
            if char == '#':
                result.append(create_point(x,y,0))
    return result

def cycle_points(points):
    result = set()

    for point in points:
        neighbours = get_neighbours(point)
        neighbours.append(point)
        for point_to_check in neighbours:
            adjecent_active_points = [p for p in get_neighbours(point_to_check) if p in points]

            if point_to_check in points:
                # it is an active cube
                if len(adjecent_active_points) == 2 or len(adjecent_active_points) == 3:
                    result.add(point_to_check)
            else:
                if len(adjecent_active_points) == 3:
                    result.add(point_to_check)

    return result