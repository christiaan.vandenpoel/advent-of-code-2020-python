# Advent of Code - Day 22 - Part One

def result(input):
    player_1, player_2 = parse_input(input)

    while len(player_1) > 0 and len(player_2) > 0:
        player_1, player_2 = play(player_1, player_2)

    won = player_1
    if len(player_1) == 0:
        won = player_2

    accumulator = calculate_score(won)

    return accumulator


def calculate_score(won):
    accumulator = 0
    for idx, value in enumerate(reversed(won)):
        accumulator += (idx + 1) * value
    return accumulator


def play(player_1, player_2):
    if player_1[0] > player_2[0]:
        return player_1[1:] + player_1[0:1] + player_2[0:1], player_2[1:]
    return player_1[1:], player_2[1:] + player_2[0:1] + player_1[0:1]

def parse_input(input):
    player_1 = []
    player_2 = []

    player = player_1

    for row in input:
        if row == "Player 2:":
            player = player_2
        elif row.isdigit():
            player.append(int(row))

    return player_1, player_2
