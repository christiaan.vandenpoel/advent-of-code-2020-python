# Advent of Code - Day 22 - Part Two
from pprint import pprint
import base64
from aoc.day22 import part1

def result(input):
    player_1, player_2 = part1.parse_input(input)

    player_1, player_2 = play(player_1, player_2)
    won = player_1
    if len(player_1) == 0:
        won = player_2

    accumulator = part1.calculate_score(won)

    return accumulator

def play(player_1, player_2, game=1):
    previous_matches = []

    round = 1
    while len(player_1) > 0 and len(player_2) > 0:
        card_1, card_2 = player_1[0], player_2[0]

        if tobase64(player_1, player_2) in previous_matches:
            # player 1 wins
            return player_1, player_2
        elif len(player_1[1:]) >= card_1 and len(player_2[1:]) >= card_2:
            # do recursive combat
            p1, p2 = play(player_1[1:card_1+1], player_2[1:card_2+1], game + 1)
            previous_matches.append(tobase64(player_1, player_2))
            if len(p1) > 0:
                # player 1 won
                player_1, player_2 = player_1[1:] + player_1[0:1] + player_2[0:1], player_2[1:]
            else:
                player_1, player_2 = player_1[1:], player_2[1:] + player_2[0:1] + player_1[0:1]
        else:
            # normal game
            # ...
            previous_matches.append(tobase64(player_1, player_2))
            if card_1 > card_2:
                player_1, player_2 = player_1[1:] + player_1[0:1] + player_2[0:1], player_2[1:]
            else:
                player_1, player_2 = player_1[1:], player_2[1:] + player_2[0:1] + player_1[0:1]

        round += 1

    return player_1, player_2

def tobase64(player1, player2):
    message = "[{},{}]".format(player1,player2)
    message_bytes = message.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    encoded = base64_bytes.decode('ascii')
    return encoded
