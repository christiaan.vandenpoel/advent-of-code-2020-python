from aoc.day21 import part1, part2

example_input = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(example_input) == 5

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(None) == None
