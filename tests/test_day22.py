from aoc.day22 import part1, part2

first_example_input = """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(first_example_input) == 306

def test_play_cards():
    player_1 = [9, 2, 6, 3, 1]
    player_2 = [5, 8, 4, 7, 10]

    (player_1, player_2) = part1.play(player_1, player_2)
    assert player_1 == [2, 6, 3, 1, 9, 5]
    assert player_2 == [8, 4, 7, 10]

    (player_1, player_2) = part1.play(player_1, player_2)
    assert player_1 == [6, 3, 1, 9, 5]
    assert player_2 == [4, 7, 10, 8, 2]

def test_parse_input():
    player_1, player_2 = part1.parse_input(first_example_input)

    assert player_1 == [9, 2, 6, 3, 1]
    assert player_2 == [5, 8, 4, 7, 10]

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_example_input) == 291
