from pprint import pprint

import pytest

from aoc.day17 import part1, part2

first_example_input = """.#.
..#
###
""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(first_example_input, 6) == 112

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_example_input, 6) == 848

def test_points():
    p1 = part1.create_point(0,0,0)
    p2 = part1.create_point(0,0,0)
    p3 = part1.create_point(0,1,0)
    assert p1 == p2
    assert p1 != p3
    assert p2 != p3

def test_point_neighbours():
    point = part1.create_point(1,1,1)
    neighbours = part1.get_neighbours(point)
    pprint(neighbours)
    assert len(neighbours) == 26

    for x in [0,1,2]:
        for y in [0,1,2]:
            for z in [0,1,2]:
                this_point = part1.create_point(x,y,z)
                if point == this_point:
                    continue
                assert this_point in neighbours

def test_input_parser():
    points = part1.get_points_from_input(first_example_input)
    pprint(points)

    assert part1.create_point(0,1,0) in points
    assert part1.create_point(1,2,0) in points
    assert part1.create_point(2,0,0) in points
    assert part1.create_point(2,1,0) in points
    assert part1.create_point(2,2,0) in points

def test_first_cycle():
    start = part1.get_points_from_input(first_example_input)

    end = part1.cycle_points(start)

    assert len(end) == 11

def test_part2_first_cycle():
    start = part2.get_points_from_input(first_example_input)
    end = part2.cycle_points(start)

    assert len(end) == 29


