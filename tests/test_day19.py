import pytest

from aoc.day19 import part1, part2

first_example_input = """0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb""".splitlines()

#
# --- Part One ---
#

@pytest.mark.skip
def test_part1():
    assert part1.result(first_example_input) == 2

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(None) == None
