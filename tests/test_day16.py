from aoc.day16 import part1, part2
import pytest

test_ticket = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12
""".splitlines()

test_ticket2 = """class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9""".splitlines()

#
# --- Part One ---
#
# @pytest.mark.skip
def test_part1_with_test_input():
    assert part1.result(test_ticket) == 71

#
# --- Part Two ---
#

def test_part2_with_test_input():
    assert part2.result(test_ticket2, "row") == 11
    assert part2.result(test_ticket2, "class") == 12
    assert part2.result(test_ticket2, "seat") == 13
    assert part2.result(test_ticket2, "(row|class)") == 132


