import pytest

from aoc.day24 import part1, part2, point

first_test_example = """sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew""".splitlines()

#
# --- Part One ---
#

def test_part1():
    assert part1.result(["nwwswee"]) == 1
    assert part1.result(first_test_example) == 10

def test_movements():
    assert part1.point.move((1,3), "e") == (2,3)
    assert part1.point.move((3,3), "w") == (2,3)

    assert part1.point.move((2,3), "ne") == (3,2)
    assert part1.point.move((2,4), "ne") == (2,3)

    assert part1.point.move((3,3), "se") == (4,4)
    assert part1.point.move((4,4), "se") == (4,5)

    assert part1.point.move((4,4), "nw") == (3,3)
    assert part1.point.move((3,3), "nw") == (3,2)

    assert part1.point.move((4,4), "sw") == (3,5)
    assert part1.point.move((3,5), "sw") == (3,6)

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(first_test_example, 0) == 10
    assert part2.result(first_test_example, 1) == 15
    assert part2.result(first_test_example, 2) == 12
    assert part2.result(first_test_example, 100) == 2208

def test_tile_neighbours():
    p = (3, 3)
    neighbours = part2.point.neighbours(p)

    assert len(neighbours) == 6
    assert (4, 2) in neighbours
    assert (4, 3) in neighbours
    assert (4, 4) in neighbours
    assert (3, 4) in neighbours
    assert (2, 3) in neighbours
    assert (3, 2) in neighbours
