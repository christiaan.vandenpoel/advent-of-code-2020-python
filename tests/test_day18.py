import pytest

from aoc.day18 import part1, part2

#
# --- Part One ---
#

def test_part1():
    assert part1.result(["1 + 2"]) == 3
    assert part1.result(["11 + 22"]) == 33
    assert part1.result(["(1 + 2)"]) == 3
    assert part1.result(["((1 + 2) + 3)"]) == 6
    assert part1.result(["(1 + 2) * 3"]) == 9
    assert part1.result(["1 + 2 * 3"]) == 9
    assert part1.result(["(1 + 2) * 3"]) == 9
    assert part1.result(["1 + (2 * 3)"]) == 7
    assert part1.result(["(1 + (2 * 3))"]) == 7
    assert part1.result(["1 + 2 * 3 + 4 * 5 + 6"]) == 71
    assert part1.result(["1 + 2 * 3 + 4 * 5 + 6"]) == 71
    assert part1.result(["1 + (2 * 3) + (4 * (5 + 6))"]) == 51
    assert part1.result(["2 * 3 + (4 * 5)"]) == 26
    assert part1.result(["5 + (8 * 3 + 9 + 3 * 4 * 3)"]) == 437
    assert part1.result(["5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"]) == 12240
    assert part1.result(["((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"]) == 13632
    assert part1.result(["2 * 3 + (4 * 5)", "5 + (8 * 3 + 9 + 3 * 4 * 3)"]) == 463

#
# --- Part Two ---
#

def test_part2():
    assert part2.result(["1 + (2 * 3) + (4 * (5 + 6))"]) == 51
    assert part2.result(["2 * 3 + (4 * 5)"]) == 46
    assert part2.result(["5 + (8 * 3 + 9 + 3 * 4 * 3)"]) == 1445
    assert part2.result(["5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"]) == 669060
    assert part2.result(["((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"]) == 23340

